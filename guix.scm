;; Spyglass server -- Minimal server for servers monitoring..
;;
;; Copyright (C) 2018 by Pierre-Antoine Rouby <contact@parouby.fr>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(use-modules ((guix licenses) #:prefix license:)
             (guix packages)
             (guix download)
             (guix git-download)
             (guix gexp)
             (guix utils)
             (guix build-system gnu)
             (gnu packages)
             (gnu packages guile)
             (gnu packages autotools)
             (gnu packages pkg-config))

(package
  (name "spyglass-server")
  (version "0.1.0")
  (source (local-file "." "spyglass-server"
                      #:recursive? #t))
  (build-system gnu-build-system)
  (native-inputs
   `(("autoconf" ,autoconf)
     ("automake" ,automake)
     ("pkg-config" ,pkg-config)))
  (propagated-inputs
   `(("guile" ,guile-2.2)
     ("guile-json" ,guile-json)))
  (home-page "https://framagit.org/prouby/spyglass-server")
  (synopsis "Server spyglass http.")
  (description "Spyglass-server is a minimal server for servers
monitoring. Get system information in json format.")
  (license license:gpl3+))
