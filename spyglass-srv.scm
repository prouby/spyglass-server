;;; spyglass-srv.scm --- Spyglass server.            -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2018 by Pierre-Antoine Rouby <contact@parouby.fr>
;;
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(use-modules (ice-9 match)
             (ice-9 regex)
             (ice-9 rdelim)
             (ice-9 popen)
             (web request)
             (web server)
             (web uri)
             (json))

;;; Authentification tokken
(define tokken (car (last-pair (command-line))))

;;; Data function
(define (uptime)
  (let ((port (open-input-file "/proc/uptime")))
    (read port)))

(define (loadavg)
  (let ((port (open-input-file "/proc/loadavg")))
    (read-line port)))

(define (diskstats)
  (define (read-all port)
    (let ((line (read-line port)))
      (cond
       ((eof-object? line)
        '())
       ((or (string-match "/ " line)
            (string-match "/home" line)
            (string-match "/srv" line))
        (cons line (read-all port)))
       (else
        (read-all port)))))

  (define (remove-useless l)
    (cond
     ((equal? l '())
      "")
     ((equal? (car l) "")
      (remove-useless (cdr l)))
     (else
      (car l))))

  (define (split-and-remove str)
    (let ((lst (string-split str #\ )))
      (format "~a,~a;" (car lst) (remove-useless (cdr lst)))))

  (let* ((port (open-input-pipe "df -h --output=target,avail"))
         (lines (read-all port)))
    (close-pipe port)
    (apply string-append
           (map split-and-remove lines))))

;;; Handler
(define (my-handler request request-body)
  (define request-path
    (uri-path (request-uri request)))

  (display request-path) (newline)

  (match (request-method request)
    ('GET
     (cond
      ((string= request-path (string-append "/" tokken "/status.json"))
       (let ((stat `(("status" . "UP")
                     ("uptime" . ,(uptime))
                     ("load"   . ,(loadavg))
                     ("disk"   . ,(diskstats)))))
         (values '((content-type . (application/json)))
                 (scm->json-string stat))))
      (else
       (display "error invalid path !\n")
       (values '((content-type . (text/plain)))
               "404 not found"))))
    (_
     (display "error invalid request !\n")
     (values '((content-type . (text/plain)))
             "404 not found"))))

;;; Run server
(run-server my-handler)
